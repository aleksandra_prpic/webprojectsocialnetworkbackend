var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var multer  = require('multer');
var mongoose = require('mongoose');
mongoose.connect('localhost:27017/projekat');

var auth = require('./routes/auth');
var index = require('./routes/index');
var users = require('./routes/users');
var projects = require('./routes/projects');

var app = express();

function imageFilter (req, file, cb) {
    // accept image only
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/uploads', express.static('uploads'));
app.use('/assets', express.static('assets'));
app.use(multer({ dest: 'uploads/', fileFilter: imageFilter }).any());


// Headers for cross-origin access
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*'); //Will change to actual Internal network IP
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, PUT, DELETE, OPTIONS');
    next();
});

app.use('/auth', auth);
app.use('/', index);
app.use('/users', users);
app.use('/projects', projects);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.status(404).json({
       message: 'not found'
  });
});


module.exports = app;
