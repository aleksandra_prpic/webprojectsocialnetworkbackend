"use strict";
var express = require('express');
var router = express.Router();
const MODEL_PATH = '../models';
var Project = require(MODEL_PATH + '/projects');
var User = require(MODEL_PATH + '/users');
var Comment = require(MODEL_PATH + '/comments');
var jwt = require('jsonwebtoken');
var multer = require('multer');

//Helper function(s)
var helpers = require('../util/helpers');


router.get('/', function (req, res, next) {
    if(req.query.name
        && req.query.name.toLocaleLowerCase() == 'true'
        && req.query.comments
        && req.query.comments.toLocaleLowerCase() == 'true'
        && req.query.count
        && parseInt(req.query.count) > 0){
        Project.find({})
            .sort('-createdTime')
            .limit(parseInt(req.query.count))
            .populate('user', 'username firstName lastName avatar')
            .populate({
                path: 'comments',
                model: 'Comment',
                populate : {
                    path: 'user',
                    model: 'User',
                    select: 'username firstName lastName avatar'
                }
            })
            .exec(function (err, projects) {
                if (err)
                    return res.status(500).json({
                        message: 'Error while fetching all projects.',
                        obj: err
                    });
                return res.status(200).json(projects);
            });
    }
    else if(req.query.name && req.query.name.toLocaleLowerCase() == 'true'){
        if(req.query.count && parseInt(req.query.count) > 0){
            if(req.query.random && req.query.random.toLocaleLowerCase() == 'true'){
                Project.count({},function (err,count) {
                    if (err)
                        return res.status(500).json({
                            message: 'Error while fetching projects.',
                            obj: err
                        });
                    Project.find({})
                        .skip(helpers.getRandomArbitrary(1, count-parseInt(req.query.count)))
                        .limit(parseInt(req.query.count))
                        .populate('user', 'username firstName lastName')
                        .exec(function (err,projects) {
                        if (err)
                            return res.status(500).json({
                                message: 'Error while fetching all projects.',
                                obj: err
                            });
                        return res.status(200).json(projects);
                    });
                });
            }else {
                Project.find({})
                    .limit(parseInt(req.query.count))
                    .populate('user', 'username firstName lastName')
                    .exec(function (err, projects) {
                    if (err)
                        return res.status(500).json({
                            message: 'Error while fetching all projects.',
                            obj: err
                        });
                    return res.status(200).json(projects);
                });
            }
        }else {
            Project.find({})
                .populate('user', 'username firstName lastName')
                .exec(function (err, projects) {
                if (err)
                    return res.status(500).json({
                        message: 'Error while fetching all projects.',
                        obj: err
                    });
                return res.status(200).json(projects);
            });
        }
    } else {
        if(req.query.count && parseInt(req.query.count) > 0){
            if(req.query.random && req.query.random.toLocaleLowerCase() == 'true'){
                Project.count({},function (err,count) {
                    if (err)
                        return res.status(500).json({
                            message: 'Error while fetching projects.',
                            obj: err
                        });
                    Project.find({})
                        .skip(helpers.getRandomArbitrary(1, count-parseInt(req.query.count)))
                        .limit(parseInt(req.query.count))
                        .exec(function (err,projects) {
                        if (err)
                            return res.status(500).json({
                                message: 'Error while fetching all projects.',
                                obj: err
                            });
                        return res.status(200).json(projects);
                    });
                });
            } else {
                Project.find({})
                    .limit(parseInt(req.query.count))
                    .exec(function (err, projects) {
                    if (err)
                        return res.status(500).json({
                            message: 'Error while fetching all projects.',
                            obj: err
                        });
                    return res.status(200).json(projects);
                });
            }
        } else {
            Project.find({}, function (err, projects) {
                if (err)
                    return res.status(500).json({
                        message: 'Error while fetching all projects.',
                        obj: err
                    });
                return res.status(200).json(projects);
            });
        }
    }
});

router.get('/:projectId', function (req, res, next) {
    Project.findById(req.params.projectId)
        .populate('user', 'username firstName lastName avatar')
        .populate({
            path: 'comments',
            model: 'Comment',
            populate : {
                path: 'user',
                model: 'User',
                select: 'username firstName lastName avatar'
            }
        })
        .exec(function (err, projects) {
            if (err)
                return res.status(500).json({
                    message: 'Error while fetching all projects.',
                    obj: err
                });
            return res.status(200).json(projects);
        });
});

//Make sure that user is logged in
router.use('/', function (req, res, next) {
    jwt.verify(req.query.token, 'superSecret', function (err, decoded) {
        if (err)
            return res.status(401).json({
                message: 'Not authenticated',
                error: err
            });
        next();
    });
});

router.post('/', function (req, res, next) {
    if (!req.body.title) {
        return res.status(500).json({
            message: 'Title is required.'
        });
    } else if (req.body.title.length < 10 || req.body.title.length > 100)
        return res.status(500).json({
            message: "Title must have between 10 and 100 characters"
        });
    if (!req.body.content) {
        return res.status(500).json({
            message: 'Content is required.'
        });
    }

    var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    if (req.body.link && !re.test(req.body.link)) {
        return res.status(500).json({
            message: 'Wrong URL'
        });
    }
    if(req.body.link) {
        if(req.body.link.indexOf('http://') !== -1|| req.body.link.indexOf('https://') !== -1) {
            var link = req.body.link
        } else {
            var link = 'http://' + req.body.link;
        }
    }
    Project.findOne({'title': req.body.title.toLowerCase()}, function (err, project) {
        if (err)
            return res.status(500).json({
                message: 'Error while checking if project is unique.'
            });
        if (project)
            return res.status(500).json({
                message: 'Project with that title already exists.'
            });
        var decoded = jwt.decode(req.query.token);
        var project = new Project({
            title: req.body.title.toLowerCase(),
            content: req.body.content,
            link: link || '',
            user: decoded._id
        });
        if(req.files && req.files[0])
            project.imageUrl = req.files[0].destination.toString().concat(req.files[0].filename);
        project.save(function (err, project) {
            if (err)
                return res.status(500).json({
                    message: 'Error while adding new project.',
                    obj: err
                });
            User.findById(project.user, function (err, user) {
                if (err)
                    return res.status(500).json({
                        message: 'Error while adding new project.',
                        obj: err
                    });

                user.projects.push(project._id);
                user.save(function (err) {

                    if (err)
                        return res.status(500).json({
                            message: 'Error while adding new project to users array.'
                        });

                    return res.status(200).json(project);
                });
            });
        });
    });
});

router.put('/:projectId', function (req, res, next) {

    if (req.body.title && (req.body.title.length < 10 || req.body.title.length > 100))
        return res.status(500).json({
            message: "Title must have between 10 and 100 characters"
        });

    var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    if (req.body.link && !re.test(req.body.link) && req.body.link !== 'empty') {
        return res.status(500).json({
            message: 'Wrong URL'
        });
    }
    if(req.body.link && req.body.link !== 'empty') {
        if(req.body.link.indexOf('http://') !== -1|| req.body.link.indexOf('https://') !== -1) {
            var link = req.body.link
        } else {
            var link = 'http://' + req.body.link;
        }
    }
    Project.findById(req.params.projectId, function (err, project) {
        if (err)
            return res.status(500).json({
                message: 'Error while finding project by id.',
                obj: err
            });
        if (!project)
            return res.status(500).json({
                message: 'Can not find that project.'
            });
        var decoded = jwt.decode(req.query.token);
        if (!(decoded._id == project.user))
            return res.status(500).json({
                message: 'You are not authorised to modify this project.',
                obj: err
            });

        if(req.body.title)
            project.title = req.body.title.toLowerCase();
        if(req.body.content)
            project.content = req.body.content;
        if(req.body.link)
            project.link = link || '';
        if(req.files && req.files[0])
            project.imageUrl = req.files[0].destination.toString().concat(req.files[0].filename);

        project.save(function (err, data) {
            if (err)
                return res.status(500).json({
                    message: 'Error while saving modified project.',
                    obj: err
                });
            res.status(200).json(data);
        });
    });
});

router.delete('/:projectId', function (req, res, next) {
    Project.findById(req.params.projectId, function (err, project) {
        if (err)
            return res.status(500).json({
                message: 'Error while finding project by id.',
                obj: err
            });
        if (!project)
            return res.status(500).json({
                message: 'Can not find that project.'
            });
        var decoded = jwt.decode(req.query.token);
        if (!(decoded._id == project.user))
            return res.status(500).json({
                message: 'You are not authorised to delete this post.',
                obj: err
            });
        project.remove(function (err) {
            if (err)
                return res.status(500).json({
                    message: 'Error while deleting project.'
                });
            res.status(200).json({
                message: 'Project successfully deleted.'
            });
        });
    });
});


router.get('/:projectId/comments', function (req, res, next) {
    Project.findById(req.params.projectId).populate('comments').exec(function (err, project) {
        if (err)
            return res.status(500).json({
                message: 'Error while finding project by id.',
                obj: err
            });
        if (!project) {
            return res.status(500).json({
                message: 'Can not find that project.'
            });
        }
        return res.status(200).json(project.comments);
    });
});

router.post('/:projectId/comments', function (req, res, next) {
    if (!req.body.content) {
        return res.status(500).json({
            message: 'Comment message is required.'
        });
    } else if (!req.body.content) {
        return res.status(500).json({
            message: 'Comment can not be longer than 600 characters.'
        })
    }
    Project.findById(req.params.projectId, function (err, project) {
        if (err)
            return res.status(500).json({
                message: 'Error occured while finding project.'
            });
        if (!project) {
            return res.status(500).json({
                message: 'Can not find that project.'
            });
        }
        var decoded = jwt.decode(req.query.token);
        var comment = new Comment({
            content: req.body.content,
            project: req.params.projectId,
            user: decoded._id
        });

        comment.save(function (err, comment) {
            if (err)
                return res.status(500).json({
                    message: 'An error occured while saving comment.',
                    obj: err
                });
            User.findById(comment.user, function (err, user) {
                if (err)
                    return res.status(500).json({
                        message: 'Error while finding user.',
                        obj: err
                    });
                user.comments.push(comment._id);
                user.save(function (err) {
                    if (err)
                        return res.status(500).json({
                            message: 'Error while adding new comment to users array.'
                        });
                    Project.findById(comment.project, function (err, project) {
                        if (err)
                            return res.status(500).json({
                                message: 'Error while finding project.',
                                obj: err
                            });
                        project.comments.push(comment._id);
                        project.save(function (err) {
                            if (err)
                                return res.status(500).json({
                                    message: 'Error while saving project.'
                                });
                            res.status(200).json(comment);
                        });
                    });
                });
            });
        });
    });
});

router.delete('/:projectId/:commentId', function (req, res, next) {
    Project.findById(req.params.projectId, function (err, project) {
        if (err)
            return res.status(500).json({
                message: 'Error while finding project by id.',
                obj: err
            });
        if (!project)
            return res.status(500).json({
                message: 'Can not find that project.'
            });
        Comment.findById(req.params.commentId, function (err, comment) {
            if (err)
                return res.status(500).json({
                    message: 'Error while finding comment by id.',
                    obj: err
                });
            if (!comment)
                return res.status(500).json({
                    message: 'Can not find that comment.'
                });
            var decoded = jwt.decode(req.query.token);
            if (!(decoded._id == comment.user))
                return res.status(500).json({
                    message: 'You are not authorised to delete this comment.',
                    obj: err
                });
            comment.remove(function (err) {
                if (err)
                    return res.status(500).json({
                        message: 'Error while deleting comment.'
                    });
                project.comments.pull(comment._id);
                project.save(function (err) {
                    if (err)
                        return res.status(500).json({
                            message: 'Error while deleting comment.'
                        });
                    return res.status(200).json({
                        message: 'Comment succesfully deleted.'
                    });
                });
            });
        });
    });
});

router.put('/:projectId/:commentId', function (req, res, next) {
    if (!req.body.content) {
        return res.status(500).json({
            message: 'Content is required.'
        });
    } else if (!req.body.content) {
        return res.status(500).json({
            message: 'Comment can not be longer than 600 characters.'
        })
    }
    Project.findById(req.params.projectId, function (err, project) {
        if (err)
            return res.status(500).json({
                message: 'Error while finding project by id.',
                obj: err
            });
        if (!project)
            return res.status(500).json({
                message: 'Can not find that project.'
            });
        Comment.findById(req.params.commentId, function (err, comment) {
            if (err)
                return res.status(500).json({
                    message: 'Error while finding comment by id.',
                    obj: err
                });
            if (!comment)
                return res.status(500).json({
                    message: 'Can not find that comment.'
                });
            var decoded = jwt.decode(req.query.token);
            if (!(decoded._id == comment.user))
                return res.status(500).json({
                    message: 'You are not authorised to modify this comment.',
                    obj: err
                });
            comment.content = req.body.content;
            comment.save(function (err, data) {
                if (err)
                    return res.status(500).json({
                        message: 'Error while saving modified user.',
                        obj: err
                    });
                res.status(200).json(data);
            });
        });
    });
});

module.exports = router;