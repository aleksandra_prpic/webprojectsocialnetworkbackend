"use strict";
var express = require('express');
var router = express.Router();
var Project = require('../models/projects');

router.get('/', function(req, res, next) {
    Project.find({},function (err,projects) {
        if(err)
            return res.status(500).json({
                message: 'Error while fetching all projects.',
                obj: err
            });
        res.status(200).json(projects);
    });
});

module.exports = router;
