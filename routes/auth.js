"use strict";
var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var User = require('../models/users');


router.post('/register', function (req, res, next) {
    if (!req.body.username)
        return res.status(500).json({
            message: 'Username is required.'
        });
    else if (!req.body.firstName)
        return res.status(500).json({
            message: 'First name is required.'
        });
    else if (!req.body.lastName)
        return res.status(500).json({
            message: 'Last name is required.'
        });
    else if (req.body.username.length < 4 || req.body.username.length > 15)
        return res.status(500).json({
            message: "Username must have between 4 and 15 characters."
        });
    else if (req.body.username.indexOf(" ") !== -1)
        return res.status(500).json({
            message: "Username can not contain space."
        });
    if (!req.body.password)
        return res.status(500).json({
            message: 'Password is required.'
        });
    else if (req.body.password.length < 6 || req.body.password.length > 20)
        return res.status(500).json({
            message: "Password must hawe between 6 and 20 characters."
        });
    if (!req.body.email)
        return res.status(500).json({
            message: 'Email is required.'
        });

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (req.body.email && !re.test(req.body.email))
        return res.status(500).json({
            message: 'Wrong email.'
        });

    User.findOne({'username': req.body.username}, function (err, user) {
        if (err)
            return res.status(500).json({
                message: 'Error while find that user.',
                obj: err
            });
        if (user)
            return res.status(500).json({
                message: 'That username is already in use. Choose another username.'
            });
        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(req.body.password, salt, function (err, hash) {
                if (err)
                    return res.status(500).json({
                        message: 'Error while crypting password.',
                        obj: err
                    });
                var newUser = new User({
                    username: req.body.username,
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    password: hash,
                    email: req.body.email
                });
                newUser.save(function (err, user) {
                    if (err)
                        return res.status(500).json({
                            message: 'Error while saving new user.',
                            obj: err
                        });
                    var myToken = jwt.sign(user.toObject(), 'superSecret', {
                        expiresIn: 14400
                    });
                    res.status(200).send({
                        'token': myToken,
                        'username': newUser.username,
                        'userFirstName': newUser.firstName,
                        'userLastName': newUser.lastName
                    });
                });
            });
        });
    });
});

router.post('/login', function (req, res, next) {

    if (!req.body.username)
        return res.status(500).json({
            message: 'Username is required.'
        });
    else if (req.body.username.length < 4 || req.body.username.length > 15)
        return res.status(500).json({
            message: "Username must have between 4 and 15 characters."
        });
    else if (req.body.username.indexOf(" ") !== -1)
        return res.status(500).json({
            message: "Username can not contain space."
        });

    if (!req.body.password)
        return res.status(500).json({
            message: 'Password is required.'
        });
    else if (req.body.password.length < 6 || req.body.password.length > 20)
        return res.status(500).json({
            message: "Password must hawe between 6 and 20 characters."
        });

    User.findOne({'username': req.body.username}, function (err, user) {
        if (err)
            return res.status(500).json({
                message: 'Error while find that user.',
                obj: err
            });
        if (!user)
            return res.status(500).json({
                message: 'Invalid login.'
            });
        bcrypt.compare(req.body.password, user.password, function (err, valid) {
            if (err)
                return res.status(500).json({
                    message: 'Error while checking your password.'
                });

            if (valid) {
                var myToken = jwt.sign(user.toObject(), 'superSecret', {
                    expiresIn: 14400
                });
                return res.status(200).send({
                    'token': myToken,
                    'userId': user._id,
                    'userFirstName': user.firstName,
                    'userAvatar': user.avatar,
                    'userLastName': user.lastName,
                    'username': user.username
                });
            } else {
                return res.status(401).json({
                    message: 'Invalid login.'
                });
            }
        });
    });
});


module.exports = router;