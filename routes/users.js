"use strict";
var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Project = require('../models/projects');
var Comment = require('../models/comments');
var User = require('../models/users');

//Helper function(s)
var helpers = require('../util/helpers');

/* GET users listing. */

router.get('/:userId', function(req, res, next) {
    User.findById(req.params.userId)
        .populate({
            'path': 'projects',
            'model': 'Project',
            populate: {
                'path': 'comments',
                'model': 'Comment',
                populate: {
                    'path': 'user',
                    'model': 'User',
                    'select': 'username firstName lastName avatar aboutMe'
                }
            }
        })
        .exec(function(err, user) {
            if (err)
                return res.status(500).json({
                    message: 'Error while finding user by id.',
                    obj: err
                });
            res.status(200).json(user);
        });
});

router.get('/', function(req, res, next) {
    if(req.query.count && parseInt(req.query.count) > 0){
        if(req.query.random && req.query.random.toLocaleLowerCase() == 'true'){
            User.count({},function (err,count) {
                if (err)
                    return res.status(500).json({
                        message: 'Error while fetching users.',
                        obj: err
                    });
                User.find({})
                    .skip(helpers.getRandomArbitrary(1, count-parseInt(req.query.count)))
                    .limit(parseInt(req.query.count))
                    .exec(function (err,users) {
                        if (err)
                            return res.status(500).json({
                                message: 'Error while fetching users.',
                                obj: err
                            });
                        return res.status(200).json(users.map(function (user) {
                            return {
                                username: user.username,
                                firstName: user.firstName,
                                lastName: user.lastName,
                                avatar: user.avatar,
                                email: user.email,
                                projects: user.projects,
                                aboutMe: user.aboutMe,
                                job: user.job,
                                mobileNumber: user.mobileNumber,
                                gender: user.gender,
                                _id: user._id
                            };
                        }));
                    });
            });
        }else {
            User.find({})
                .limit(parseInt(req.query.count))
                .exec(function (err, users) {
                    if (err)
                        return res.status(500).json({
                            message: 'Error while finding user by id.',
                            obj: err
                        });
                    return res.status(200).json(users.map(function (user) {
                        return {
                            username: user.username,
                            firstName: user.firstName,
                            lastName: user.lastName,
                            avatar: user.avatar,
                            email: user.email,
                            projects: user.projects,
                            aboutMe: user.aboutMe,
                            job: user.job,
                            mobileNumber: user.mobileNumber,
                            gender: user.gender,
                            _id: user._id
                        };
                    }));
                });
        }
    } else {
        User.find({})
            .exec(function(err, users) {
                if (err)
                    return res.status(500).json({
                        message: 'Error while finding user by id.',
                        obj: err
                    });
                return res.status(200).json(users.map(function (user) {
                    return {
                        username: user.username,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        avatar: user.avatar,
                        email: user.email,
                        projects: user.projects,
                        aboutMe: user.aboutMe,
                        job: user.job,
                        mobileNumber: user.mobileNumber,
                        gender: user.gender,
                        _id: user._id
                    };
                }));
            });
    }
});

//Make sure that user is logged in
router.use('/', function (req,res,next) {
    jwt.verify(req.query.token,'superSecret',function (err,decoded) {
        if(err)
            return res.status(401).json({
                message : 'Not authenticated',
                error: err
            });
        next();
    });
});

router.put('/:userId', function(req, res, next) {
    var decoded = jwt.decode(req.query.token);
    if(req.params.userId != decoded._id)
        return res.status(401).json({
            message: 'You cannot change profile information for other users than you.'
        });
    if (req.body.username && (req.body.username.length < 4 || req.body.username.length > 15))
        return res.status(500).json({
            message: "Username must have between 4 and 15 characters."
        });
    else if (req.body.username && req.body.username.indexOf(" ") !== -1)
        return res.status(500).json({
            message: "Username can not contain space."
        });
    if (req.body.job && (req.body.job.length < 3 || req.body.job.length > 30))
        return res.status(500).json({
            message: "Job must have between 3 and 30 characters."
        });
    if (req.body.mobileNumber && (req.body.mobileNumber.length < 9 || req.body.mobileNumber.length > 10))

        return res.status(500).json({
            message: "Number must have between 9 or 10 digits."
        });

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (req.body.email && !re.test(req.body.email))
        return res.status(500).json({
            message: 'Wrong email.'
        });

    User.findById(req.params.userId, function(err, user) {
        if (err)
            return res.status(500).json({
                message: 'Error while finding user by id.',
                obj: err
            });
        if(req.body.username){
            User.findOne({username: req.body.username}, function (err,userFound) {
                if (err)
                    return res.status(500).json({
                        message: 'Error while finding user by id.',
                        obj: err
                    });
                if (userFound && userFound._id != req.params.userId)
                    return res.status(500).json({
                        message: 'That username is already taken.'
                    });
                user.username = (
                    req.body.username &&
                    req.body.username.toLowerCase() != user.username.toLowerCase())?
                    req.body.username:user.username;
                user.email = (
                    req.body.email &&
                    req.body.email.toLowerCase() != user.email.toLowerCase())?
                    req.body.email:user.email;
                user.email = (
                    req.body.email &&
                    req.body.email.toLowerCase() != user.email.toLowerCase())?
                    req.body.email:user.email;
                user.firstName = (
                    req.body.firstName &&
                    req.body.firstName.toLowerCase() != user.firstName.toLowerCase())?
                    req.body.firstName:user.firstName;
                user.job = (
                    req.body.job &&
                    req.body.job.toLowerCase() != user.job.toLowerCase())?
                    req.body.job:user.job;
                if(req.body.gender && (new String(req.body.gender).valueOf() === new String('male').valueOf() || new String(req.body.gender).valueOf() === new String('female').valueOf()))
                    user.gender = req.body.gender;
                if(req.body.mobileNumber)
                    user.mobileNumber = req.body.mobileNumber;
                user.lastName = (
                    req.body.lastName &&
                    req.body.lastName.toLowerCase() != user.lastName.toLowerCase())?
                    req.body.lastName:user.lastName;
                user.aboutMe = (req.body.aboutMe)?req.body.aboutMe:user.aboutMe;
                if(req.files && req.files[0]) {
                    user.avatar = req.files[0].destination.toString().concat(req.files[0].filename);
                }

                user.save(function (err, data) {
                    if (err)
                        return res.status(500).json({
                            message: 'Error while saving modified user.',
                            obj: err
                        });
                    res.status(200).json(data);
                });
            });
        } else {
            user.username = (
                req.body.username &&
                req.body.username.toLowerCase() != user.username.toLowerCase())?
                req.body.username:user.username;
            user.email = (
                req.body.email &&
                req.body.email.toLowerCase() != user.email.toLowerCase())?
                req.body.email:user.email;
            user.email = (
                req.body.email &&
                req.body.email.toLowerCase() != user.email.toLowerCase())?
                req.body.email:user.email;
            user.firstName = (
                req.body.firstName &&
                req.body.firstName.toLowerCase() != user.firstName.toLowerCase())?
                req.body.firstName:user.firstName;
            user.job = (
                req.body.job &&
                req.body.job.toLowerCase() != user.job.toLowerCase())?
                req.body.job:user.job;
            if(req.body.gender && (new String(req.body.gender).valueOf() === new String('male').valueOf() || new String(req.body.gender).valueOf() === new String('female').valueOf()))
                user.gender = req.body.gender;
            if(req.body.mobileNumber)
                user.mobileNumber = req.body.mobileNumber;
            user.lastName = (
                req.body.lastName &&
                req.body.lastName.toLowerCase() != user.lastName.toLowerCase())?
                req.body.lastName:user.lastName;
            user.aboutMe = (req.body.aboutMe)?req.body.aboutMe:user.aboutMe;
            if(req.files && req.files[0]) {
                user.avatar = req.files[0].destination.toString().concat(req.files[0].filename);
            }

            user.save(function (err, data) {
                if (err)
                    return res.status(500).json({
                        message: 'Error while saving modified user.',
                        obj: err
                    });
                res.status(200).json(data);
            });
        }

    });
});

router.post('/:userId/changepw', function (req,res,next) {
    if(!req.body.current || !req.body.new)
        return res.status(500).json({
            message: 'Both current and new password are required'
        });
    if((req.body.current.length < 6 || req.body.new.length < 6) || (req.body.current.length > 20 || req.body.new.length > 20))
        return res.status(500).json({
            message: 'Password must have between 6 and 20 characters.'
        });
    if(req.body.current === req.body.new)
        return res.status(500).json({
            message: 'New password cannot be same as current password.'
        });
    var decoded = jwt.decode(req.query.token);
    if(req.params.userId != decoded._id)
        return res.status(401).json({
            message: 'You cannot change password for other users than you.'
        });
    User.findOne({username:decoded.username}, function(err, user) {
        if(err)
            return res.status(500).json({
                message: 'An error occurred while changing password.',
                error: err
            });
        //Verify that current password from request is correct
        if (!bcrypt.compareSync(req.body.current, user.password))
            return res.status(401).json({
                message: 'Wrong current password'
            });
        user.password = bcrypt.hashSync(req.body.new, 10);
        user.save(function (err) {
            if(err)
                return res.status(500).json({
                    message: 'An error occurred while changing password.',
                });
            return res.status(200).json({
                'message': 'Password has been changed.'
            });
        });
    });
});

module.exports = router;
