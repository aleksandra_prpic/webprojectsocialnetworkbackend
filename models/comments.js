var mongoose = require('mongoose');
var User = require('../models/users');

var schema = mongoose.Schema({
    content: {type: String, required: true},
    project: {type: mongoose.Schema.Types.ObjectId, ref: 'Project'},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    createdTime: {type: Date, default: Date.now}
});

schema.post('remove', function(doc) {
    User.update({_id:doc.user}, { $pull: { comments : doc._id } },function (){});
});

module.exports = mongoose.model('Comment', schema);