var mongoose = require('mongoose');

var schema = mongoose.Schema({
    username: {type: String, required: true},
    password: {type: String, required: true},
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    avatar: {type: String, required: false, default: 'assets/default_avatar.png'},
    email: {type: String, required: true},
    aboutMe: {type: String, required: false, default:''},
    job: {type: String, required: false, default:''},
    mobileNumber: {type: String, required: false, default:''},
    gender: {type: String, required: false, default:''},
    projects: [{type: mongoose.Schema.Types.ObjectId, ref: 'Project'}],
    comments: [{type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}]
});

module.exports = mongoose.model('User', schema);