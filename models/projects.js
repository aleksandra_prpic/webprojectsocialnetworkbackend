"use strict";
var mongoose = require('mongoose');
const MODEL_PATH = '../models';
var User = require(MODEL_PATH+'/users');
var Comment = require(MODEL_PATH+'/comments');

var schema = mongoose.Schema({
    title: {type: String, required: true},
    content: {type: String, required: true},
    link: {type: String, required: false},
    imageUrl: {type: String, required: false, default: 'assets/default_project.jpg'},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    comments: [{type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}],
    createdTime: {type: Date, default: Date.now}
});

schema.post('remove', function(doc) {
    User.findById(doc.user, function (err, user) {
        user.projects.pull(doc._id);
        user.save();
    });
    Comment.find({project:doc._id},function (err, comments) {
        if(err)
            return new Error('Error while removing comments');
        comments.forEach(function(comment) {
            comment.remove();
        })
    });
});

module.exports = mongoose.model('Project', schema);